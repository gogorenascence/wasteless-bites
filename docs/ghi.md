# GHI: Graphical Human Interface

---

## Main Page: Item List View

![main-page](wireframes/main-page.png)

The main page is either an example spash page (non-logged in users) or the item list (for logged-in users)

## Signup Form

![signup](wireframes/signup.png)

Users can register to gain access

## Login

![login](wireframes/login.png)

Registered users can log in using the log-in modal

## User Profile

![user-profile](wireframes/user-profile.png)

Profile can be edited and updated when changes are necessary; accounts may also be deleted here

## Item Detail View

![item-detail-view](wireframes/item-detail-view.png)

Clicking on an item in the main page will take you to the item's detail page

## Item Form

![item-form](wireframes/item-form.png)

Users can create new items (and delete existing ones) through forms of the same shape

## FAQ

![faq](wireframes/faq.png)

Extra context is provided to the users via the FAQ
